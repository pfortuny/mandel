/* Build the Mandelbrot set:
a,b, P1
c,d, P2

makes a ppm of the Mandelbrot set in [a,b] x [c,d]
with P1xP2 pixels
*/
/* (c) 2012 Pedro Fortuny Ayuso - BSD licence */
/* This is an example of using
   1) gmp (the GNU multiple precision library)
   2) Grand Central Dispatch (the thread manager in OS X)
*/

#include <stdio.h>
#include <alloca.h>
#include <string.h>
#include <stdlib.h>
#include <pthread.h>
#include "m_apm.h"

M_APM Four;
char **img;
struct pixel_attr {
        int XX;
        int YY;
        M_APM c_r;
        M_APM c_i;                                
};

struct pixel_attr *pixel_data;

void do_pixel(struct pixel_attr * pixel_datum) {
                        
        float count = 0;
        unsigned char r, g, b;
        int xx = pixel_datum->XX;
        int yy = pixel_datum->YY;
        int context;
        M_APM tt1,tt2,tt3,tt4,pp_r,pp_i,cc_r,cc_i;
                
                

        pp_r = m_apm_init();
        pp_i = m_apm_init();
        cc_r = m_apm_init();
        cc_i = m_apm_init();
        m_apm_copy(cc_r, pixel_datum->c_r);
        m_apm_copy(cc_i, pixel_datum->c_i);
        tt1 = m_apm_init();
        tt2 = m_apm_init();
        tt3 = m_apm_init();
        tt4 = m_apm_init();
        for(count = 0; (count < 512) ; count++){
                m_apm_multiply(tt1, pp_r, pp_r);
                m_apm_multiply(tt2, pp_i, pp_i); 
                m_apm_subtract(tt3, tt1, tt2);

                m_apm_multiply(tt1, pp_r, pp_i);
                m_apm_add(tt4, tt1, tt1);

                m_apm_copy(pp_r, tt3);
                m_apm_copy(pp_i, tt4);

                m_apm_add(tt1, pp_r, cc_r);
                m_apm_add(tt2, pp_i, cc_i);

                m_apm_copy(pp_r, tt1);
                m_apm_copy(pp_i, tt2);
                // m_apm_add(pp_r, pp_r, cc_r);
                // m_apm_add(pp_i, pp_i, cc_i);
                // p = p * p + c;
                        
                m_apm_multiply(tt1, pp_r, pp_r);
                m_apm_multiply(tt2, pp_i, pp_i);
                m_apm_add(tt3, tt1, tt2);
                if((m_apm_compare(tt3, Four)>0)){
                        break;
                }
        }
        m_apm_free(pp_r);
        m_apm_free(pp_i);
        m_apm_free(cc_r);
        m_apm_free(cc_i);
        m_apm_free(tt1);
        m_apm_free(tt2);
        m_apm_free(tt3);
        m_apm_free(tt4);

        int c = (int )count;
        r = 255-255/(c+1); //255 - ((count < 255) ? count : 255);
        c = c % 256;
        g = 255 - 255/(c+1); //* (((int )count) % 128); //0; //count % 255;
        //c>>=1;
        b = 255 - (c % 128);
        //b = 255 - c/2;
        //b = 128-c ;
        snprintf(img[xx], 15, "%i %i %i", r, g, b);
};


int main(int argc, char *argv[]){

	char *OX, *OY;
	unsigned int xw, yw;
        M_APM xm, xM, ym, yM, xstep, ystep;
        unsigned int DIGITS;

        Four = m_apm_init();
        m_apm_set_string(Four, "4");
	//mpf_t xm, xM, ym, yM, xstep, ystep;
        
        xm = m_apm_init();
        xM = m_apm_init();
        ym = m_apm_init();
        yM = m_apm_init();
        xstep = m_apm_init();
        ystep = m_apm_init();
        
        /* Grand Central Dispatch stuff */
        DIGITS = 64;
        /*mpf_set_default_prec(64);*/

	OX = calloc(1, 1000);
	OY = calloc(1, 1000);
	fgets(OX, 1000, stdin);
	fgets(OY, 1000, stdin);
	char *token;
	token = strtok(OX, ",");
        m_apm_set_string(xm, token);
	token = strtok(NULL, ",");
        m_apm_set_string(xM, token);
	token = strtok(NULL, ",");
	xw = atoi(token);

	token = strtok(OY, ",");
        m_apm_set_string(ym, token);
	token = strtok(NULL, ",");
        m_apm_set_string(yM, token);
	token = strtok(NULL, ",");
	yw = atoi(token);

        img = (char **)calloc(xw, sizeof(char *));
        
        int l,m;
        for(l=0; l<xw; l++){
                img[l] = (char *)calloc(16, sizeof(char ));
        }

        M_APM t1,t2,t3,t4;
        t1 = m_apm_init();
        t2 = m_apm_init();
        t3 = m_apm_init();
        t4 = m_apm_init();


        m_apm_subtract(t1, xM, xm);
        m_apm_set_double(t2, xw);
        DIGITS = 2*abs(m_apm_exponent(t1));
        m_apm_divide(xstep, DIGITS, t1, t2);

        m_apm_subtract(t1, yM, ym);
        m_apm_set_double(t2, yw);
        m_apm_divide(ystep, DIGITS, t1, t2);
	
        // set all to new precision
        printf("P3\n");
        printf("%i %i\n", xw, yw);
        printf("255\n");


	// int image[xw][yw];
	M_APM x, y;
        x = m_apm_init();
        y = m_apm_init();

        int Y;
        pthread_t pixel[xw];

        pixel_data = (struct pixel_attr *)calloc(xw, sizeof(pixel_data));
        M_APM Y_l, X_l;
        Y_l = m_apm_init();
        X_l = m_apm_init();
        for(Y=yw-1; Y>=0; Y--){

                m_apm_set_double(Y_l, Y);
                m_apm_multiply(t1, ystep, Y_l);
                m_apm_subtract(y, yM, t1);
                // y = yM -ystep *Y;
                int X;
		for(X=0; X<xw; X++){

                        int pos = X*yw + Y;
                        m_apm_set_double(X_l, X);
                        m_apm_multiply(t1, xstep, X_l);
                        m_apm_add(x, xm, t1);
                        pixel_data[X].XX = X;
                        pixel_data[X].YY = Y;
                        pixel_data[X].c_r = m_apm_init();
                        pixel_data[X].c_i = m_apm_init();
                        m_apm_copy(pixel_data[X].c_r, x);
                        m_apm_copy(pixel_data[X].c_i, y);

                        int free;
                        int X1 = X;
                        int X2 = Y;
                        
                        pthread_create(&pixel[X], NULL, do_pixel, &pixel_data[X]);
                        
		}
                int u;
                for(u=0; u<xw; u++){
                        pthread_join(pixel[u], NULL);
                }

                int j;
                for(j=0; j<xw; j++){
                        printf("%s  ", img[j]);
                }
                printf("\n");
        }
        
        exit(0);
}



