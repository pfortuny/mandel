/* Build the Mandelbrot set:
a,b, P1
c,d, P2

makes a ppm of the Mandelbrot set in [a,b] x [c,d]
with P1xP2 pixels
*/
/* (c) 2012 Pedro Fortuny Ayuso - BSD licence */
/* This is an example of using
   1) gmp (the GNU multiple precision library)
   2) Grand Central Dispatch (the thread manager in OS X)
*/


#include <stdio.h>
#include <alloca.h>
#include <string.h>
#include <stdlib.h>
#include <dispatch/dispatch.h>
#include <gmp.h>


int main(int argc, char *argv[]){

	char *OX, *OY;
	unsigned int xw, yw;
	mpf_t xm, xM, ym, yM, xstep, ystep;
        __block char **img;
        
        mpf_init(xm);
        mpf_init(xM);
        mpf_init(ym);
        mpf_init(yM);
        mpf_init(xstep);
        mpf_init(ystep);
        
        /* Grand Central Dispatch stuff */
        dispatch_queue_t queue;
        queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,
                                          0);
        dispatch_group_t row_group = dispatch_group_create();
        mpf_set_default_prec(64);

	OX = calloc(1, 1000);
	OY = calloc(1, 1000);
	fgets(OX, 1000, stdin);
	fgets(OY, 1000, stdin);
	char *token;
	token = strtok(OX, ",");
        mpf_set_str(xm, token, 10);
	token = strtok(NULL, ",");
        mpf_set_str(xM, token, 10);
	token = strtok(NULL, ",");
	xw = atoi(token);

	token = strtok(OY, ",");
        mpf_set_str(ym, token, 10);
	token = strtok(NULL, ",");
        mpf_set_str(yM, token, 10);
	token = strtok(NULL, ",");
	yw = atoi(token);

        img = (char **)calloc(xw, sizeof(char *));
        
        int l,m;
        for(l=0; l<xw; l++){
                img[l] = (char *)calloc(16, sizeof(char ));
        }

        mpf_t t1,t2,t3,t4;
        mpf_init(t1);
        mpf_init(t2);
        mpf_init(t3);
        mpf_init(t4);

        mpf_sub(t1, xM, xm);
        mpf_div_ui(xstep, t1, xw);

        mpf_sub(t1, yM, ym);
        mpf_div_ui(ystep, t1, yw);
	
        long int ex;
        mpf_get_d_2exp(&ex, xstep);

        mpf_set_default_prec(64+ex);

        // set all to new precision
        mpf_init(t1);
        mpf_init(t2);
        mpf_init(t3);
        mpf_init(t4);

        printf("P3\n");
        printf("%i %i\n", xw, yw);
        printf("255\n");


	// int image[xw][yw];
	mpf_t x, y;
        mpf_init(x);
        mpf_init(y);
	//int X=0,Y=0;
        //int r,g,b;
        dispatch_semaphore_t lock = dispatch_semaphore_create(100);


        void (^loop_block)(int, int,  mpf_t,  mpf_t) =
                ^(int XX, int YY, mpf_t c_r, mpf_t c_i){
                        
                float count = 0;
                unsigned char r, g, b;
                int xx = XX;
                int yy = YY;
                int context;
                mpf_t tt1,tt2,tt3,tt4,pp_r,pp_i,cc_r,cc_i;
                
                

                mpf_init(pp_r);
                mpf_init(pp_i);
                mpf_init(cc_r);
                mpf_init(cc_i);
                mpf_set(cc_r, c_r);
                mpf_set(cc_i, c_i);
                mpf_init(tt1);
                mpf_init(tt2);
                mpf_init(tt3);
                mpf_init(tt4);
                for(count = 0; (count < 512) ; count++){
                        mpf_mul(tt1, pp_r, pp_r);
                        mpf_mul(tt2, pp_i, pp_i); 
                        mpf_sub(tt3, tt1, tt2);

                        mpf_mul(tt1, pp_r, pp_i);
                        mpf_add(tt4, tt1, tt1);

                        mpf_set(pp_r, tt3);
                        mpf_set(pp_i, tt4);

                        mpf_add(tt1, pp_r, cc_r);
                        mpf_add(tt2, pp_i, cc_i);

                        mpf_set(pp_r, tt1);
                        mpf_set(pp_i, tt2);
                        // mpf_add(pp_r, pp_r, cc_r);
                        // mpf_add(pp_i, pp_i, cc_i);
                        // p = p * p + c;
                        
                        mpf_mul(tt1, pp_r, pp_r);
                        mpf_mul(tt2, pp_i, pp_i);
                        mpf_add(tt3, tt1, tt2);
                        if((mpf_cmp_ui(tt3, 4)>0)){
                                break;
                        }
                }
                mpf_clear(pp_r);
                mpf_clear(pp_i);
                mpf_clear(cc_r);
                mpf_clear(cc_i);
                mpf_clear(tt1);
                mpf_clear(tt2);
                mpf_clear(tt3);
                mpf_clear(tt4);

                int c = (int )count;
                r = 255-255/(c+1); //255 - ((count < 255) ? count : 255);
                c = c % 256;
                g = 255 - 255/(c+1); //* (((int )count) % 128); //0; //count % 255;
                //c>>=1;
                b = 255 - c % 128;
                //b = 255 - c/2;
                //b = 128-c ;
                snprintf(img[xx], 15, "%i %i %i", r, g, b);
                dispatch_semaphore_signal(lock);
        };

        int Y;


        for(Y=yw-1; Y>=0; Y--){

                mpf_mul_ui(t1, ystep, Y);
                mpf_sub(y, yM, t1);
                // y = yM -ystep *Y;
                int X;
		for(X=0; X<xw; X++){


                        int pos = X*yw + Y;

                        mpf_mul_ui(t1, xstep, X);
                        mpf_add(x, xm, t1);
                        // x = xm + xstep *X;
                        mpf_t *cr, *ci;
                        cr = (mpf_t *)calloc(1, sizeof(mpf_t));
                        ci = (mpf_t *)calloc(1, sizeof(mpf_t));
                        mpf_init(*cr);
                        mpf_init(*ci);
                        mpf_set(*cr, x);
                        mpf_set(*ci, y);


                        int free;
                        int X1 = X;
                        int X2 = Y;
                        
                        dispatch_semaphore_wait(lock, DISPATCH_TIME_FOREVER);
                        dispatch_group_async(row_group,queue,
                                             ^{
                                                     //dispatch_semaphore_wait(lock, DISPATCH_TIME_FOREVER);
                                                     loop_block(X1, X2,*cr,*ci);}
                                );
                        
                        //mpf_clear(*cr);
                        //mpf_clear(*ci);
                        //free(cr);
                        //free(ci);
		}
                dispatch_group_wait(row_group, DISPATCH_TIME_FOREVER);
                int j;
                for(j=0; j<xw; j++){
                        printf("%s  ", img[j]);//*(*(img+j)+Y));
                }
                printf("\n");
        }
/*         int j,k; */
/*         for(k=yw-1; k>=0; k--){ */
/*                 printf("\n"); */
/* 	} */
        
        exit(0);
}



