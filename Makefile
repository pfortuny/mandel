CC=gcc
CCLINUX=clang
LIBS=-lm -lgmp
LINUXLIBS= -fblocks -lBlocksRuntime -ldispatch
default:
	$(CC) -g mandel.c -o mandel $(LIBS) 
	cat sample.800 | ./mandel > trial.ppm
	gm convert trial.ppm trial.png
	open trial.png

# this runs only on a system with MAPM lib and headers in
# ./Multiple_precision
mapm:
	$(CC) -g -I./Multiple_precision mandel_mapm.c -o mandel_mapm -LMultiple_precision -lmapm

hard:
	$(CC) -g mandel.c -o mandel -lm -lgmp
	cat sample.800 | ./mandel > 800.ppm
	gm convert 800.ppm 800.png
	open 800.png

super:
	$(CC) -g mandel.c -o mandel -lm -lgmp
	cat sample.3000 | ./mandel > 3000.ppm
	gm convert 3000.ppm 3000.png
	open 3000.png

linux:
	$(CCLINUX) -g mandel.c -o mandel $(LIBS) $(LINUXLIBS)
	cat sample.800 | ./mandel > 800.ppm

clean:
	rm -fr *~ *dSYM *o mandel  *png *ppm
