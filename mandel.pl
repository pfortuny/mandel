#!/usr/bin/perl -w
#use bignum;


# left, right, pixels
my @OX = split ',', <stdin>;
my @OY = split ',', <stdin>;

chomp (@OX, @OY);

print "[" . join (',', @OX) . "]";
print "[" . join (',', @OY) . "]\n";

my $xstep = ($OX[1]-$OX[0])/$OX[2];
my $ystep = ($OY[1]-$OY[0])/$OY[2];

print "xs: $xstep, ys: $ystep\n";

my @image;
my $count;
my ($X, $Y)=(0,0);

for (my $x=$OX[0]; $x<=$OX[1]; $x += $xstep){
  $image[$X] = [];
  $Y=0;
  for (my $y=$OY[0]; $y<=$OY[1]; $y += $xstep) {
    $Y++;
    my @c = ($x, $y);
    my @point = ($x, $y);
  COUNT:
    for ($count = 0; $count < 1000; $count++) {
      @point = iter(@point, @c);
      if (modulus(@point)>4) {
	${$image[$X]}[$Y] = $count;
	last COUNT;
      }
    }
    print "[$x, $y, $count]\n";
  }
  $X++;
}

sub iter {
  return ($_[0]*$_[0] - $_[1]*$_[1] + $_[2], $_[0]*$_[1]+$_[1]*$_[0] + $_[3]);
}

sub modulus {
  return ($_[0]**2 + $_[1]**2);
}
